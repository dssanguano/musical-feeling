import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from pysentimiento import create_analyzer
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from starlette.middleware.cors import CORSMiddleware
from RespuestaToken import obtener_respuesta
import transformers

# Crear una instancia de FastAPI
app = FastAPI()

# Definir los orígenes permitidos para las solicitudes CORS
origins = [
    "http://localhost:3000",  # Agrega aquí los orígenes permitidos (puede ser una lista)
]

# Agregar el middleware para permitir solicitudes CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Definir una clase de modelo para el input del análisis de sentimiento
class SentimientoInput(BaseModel):
    texto: str

# Función para reproducir listas de reproducción basadas en una emoción
def reproducir_lista_emocion(emocion):
    # Configurar las credenciales de la API de Spotify
    client_id = 'aed80824dfe747ab9c19e22ee05177ff'
    client_secret = '5409b8cba14342089227d7dfe23bf77b'
    client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    results = sp.search(q=emocion, type='playlist')

    # Obtener las listas de reproducción encontradas
    playlists = results['playlists']['items']

    # Crear una lista con los datos que te interesan
    playlists_info = [{"name": playlist['name'], "href": playlist['external_urls']['spotify']} for playlist in
                      playlists]

    return playlists_info

# Ruta para analizar el sentimiento de un texto
@app.post("/analizar_sentimiento/")
def analizar_sentimiento(input_data: SentimientoInput):
    # Configuración para reducir los logs de Transformers
    transformers.logging.set_verbosity(transformers.logging.ERROR)
    emocion_analyzer = create_analyzer(task="emotion", lang="es")

    texto = input_data.texto
    resultado = emocion_analyzer.predict(texto)
    emocion = resultado.output

    # Traducir las emociones a nombres más amigables
    if emocion == "sadness":
        emocion = "tristeza"
    elif emocion == "joy":
        emocion = "alegría"
    elif emocion == "fear":
        emocion = "miedo"
    elif emocion == "anger":
        emocion = "enojo"
    else:
        # Si no se reconoce la emoción, obtener una respuesta personalizada
        emocion = obtener_respuesta(texto)

    # Obtener listas de reproducción asociadas a la emoción
    playlists = reproducir_lista_emocion(emocion)

    response_data = {
        "sentimiento": emocion,
        "listas_reproduccion": playlists
    }

    return response_data

# Ruta para obtener listas de reproducción basadas en una emoción
@app.post("/sentimiento/")
def sentimiento(input_data: SentimientoInput):
    emocion = input_data.texto
    playlists = reproducir_lista_emocion(emocion)

    response_data = {
        "sentimiento": emocion,
        "listas_reproduccion": playlists
    }
    return response_data

# Ejecutar la aplicación en el servidor con Uvicorn
if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
