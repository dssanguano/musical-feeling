import openai
openai.api_key = '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'
def obtener_respuesta(texto_entrada):
    rol = "Eres un experto en encontrar una emocion de todo el texto ingresado, que no sea tristeza, alegria, miedo, enojo. En solo una palabara."
    texto_completo = f"Rol: {rol}\nUsuario: {texto_entrada}"
    respuesta = openai.Completion.create(
        engine="text-davinci-003",
        prompt=texto_completo,
        max_tokens=200,
        temperature=1,
        stop=None,
    )

    respuesta_generada = respuesta.choices[0].text.strip()

    return respuesta_generada
